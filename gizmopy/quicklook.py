import numpy as np
import matplotlib
import matplotlib.pylab as pylab
import matplotlib.pyplot as plot
from gizmopy.load_from_snapshot import *
from gizmopy.load_fire_snap import *
from scipy.optimize import curve_fit
#matplotlib.use('Agg') ## this calls matplotlib without a X-windows GUI

np.seterr(divide='ignore',over='ignore',under='ignore',invalid='ignore')


def quicklook(sdir,snum,rcut,cen=np.zeros(0),xz=False,yz=False,xy=True,findcen=False,
    key='stardmgastemp',numpart=1e9,figsize=(8.,8.),rasterized=True,marker=',',
    dt_Myr=100.,weight='B',n_bins=100,quiet=False,pdf=False,**kwargs):
    '''
    master routine to quickly examine various snapshot properties
    '''    
    numpart = np.round(numpart).astype('int')
    pylab.close('all'); cen=np.array(cen); plot.figure(1,figsize=figsize);
    numpart_infile = load_fire_snap('NumPart_Total',-1,sdir,snum)
    ptype_cen = 4 # default to search for stars
    if(numpart_infile[ptype_cen]<=0): ptype_cen = 1 # then look for high-res dm
    if(numpart_infile[ptype_cen]<=0): ptype_cen = 0 # then look for gas
    if(numpart_infile[ptype_cen]<=0): ptype_cen = 5 # then look for bhs
    if(numpart_infile[ptype_cen]<=0): ptype_cen = 2 # then look for low-res dm
    if(numpart_infile[ptype_cen]<=0): ptype_cen = 3 # then look for lower-res dm
    if(findcen):
        afac=load_fire_snap('Time',-1,sdir,snum)
        max_search = 1000.*afac/2.
        if(max_search<200.): max_search=200.
        if(max_search>1000.): max_search=1000.
        cen,n0=estimate_zoom_center(sdir,snum,ptype=ptype_cen,min_searchsize=0.005,quiet=True,
            search_deviation_tolerance=0.0005,search_stepsize=1.25,max_searchsize=max_search)
    if(cen.size<=0):
        xyz=load_fire_snap('Coordinates',ptype_cen,sdir,snum)
        cen=np.median(xyz,axis=0); cn=xyz-cen; r2=np.sum(cn*cn,axis=1); ok=np.where(r2<rcut*rcut)[0]; xyz=xyz.take(ok,axis=0); 
        if(xyz.size > 3): cen=np.median(xyz,axis=0); 
        print('no center provided, centering on ',cen)
        
    if('lightprofile' in key or 'light_profile' in key):
        quicklook_light_profile(sdir,snum,cen=cen,rcut=rcut,key=key,weight=weight)
        if(pdf): pylab.savefig('qlook_'+key+'.pdf',transparent=True)
        return
    if('massprofile' in key or 'mass_profile' in key):
        quicklook_mass_profile(sdir,snum,cen=cen,rcut=rcut,key=key,n_bins=n_bins)
        if(pdf): pylab.savefig('qlook_'+key+'.pdf',transparent=True)
        return
    if('tprofile' in key or 't_profile' in key or 'sprofile' in key or 's_profile' in key or 'temp_profile' in key or 'tempprofile' in key):
        quicklook_temp_profile(sdir,snum,cen=cen,rcut=rcut,key=key)
        if(pdf): pylab.savefig('qlook_'+key+'.pdf',transparent=True)
        return
    if('sfr' in key): 
        quicklook_sfr_time(sdir,snum,cen=cen,rcut=rcut,dt_Myr=dt_Myr,key=key)
        if(pdf): pylab.savefig('qlook_'+key+'.pdf',transparent=True)
        return
    if('phase' in key): 
        quicklook_phase(sdir,snum,cen=cen,rcut=rcut,marker=marker,key=key)
        if(pdf): pylab.savefig('qlook_'+key+'.pdf',transparent=True)
        return
    if('vc' in key):
        quicklook_vc_profile(sdir,snum,cen=cen,rcut=rcut,key=key)
        if(pdf): pylab.savefig('qlook_'+key+'.pdf',transparent=True)
        return
    if('angmom' in key):
        p,j = quicklook_j_distrib(sdir,snum,cen=cen,rcut=rcut,key=key,weight=weight)
        if(pdf): pylab.savefig('qlook_'+key+'.pdf',transparent=True)
        return
    if('Bfield' in key or 'Bmag' in key): 
        quicklook_b_vs_rho(sdir,snum,cen=cen,rcut=rcut)
        if(pdf): pylab.savefig('qlook_'+key+'.pdf',transparent=True)
        return
    if('stellarmassdistrib' in key): 
        quicklook_stellar_mass_function(sdir,snum,cen=cen,rcut=rcut,**kwargs)
        if(pdf): pylab.savefig('qlook_'+key+'.pdf',transparent=True)
        return

    xcoord=0; ycoord=1; zcoord=2; 
    if(xz):
        xcoord=0; ycoord=2; zcoord=1;
    if(yz):
        xcoord=1; ycoord=2; zcoord=0;
        
    Npart_all = load_fire_snap('NumPart_Total',-1,sdir,snum)
    print(Npart_all)

    m_dm=0; m_star=0; m_gas=0; m_cold=0; m_warm=0; m_hot=0; m_bh_tot=0;
    if('dm' in key):
        if(Npart_all[1]>0):
            xyz,mask=center_and_clip(load_fire_snap('Coordinates',1,sdir,snum),cen,rcut,numpart)
            if(xyz.size > 0):
                if not (quiet): 
                    m_dm_1 = load_fire_snap('Masses',1,sdir,snum,particle_mask=mask)
                    m_dm+=np.sum(m_dm_1)
                    if('mass_check' in key):
                        print('(High-res DM) Mass Range == ',np.min(m_dm_1),np.median(m_dm_1),np.mean(m_dm_1),np.max(m_dm_1))
                    print('DM mass=',m_dm)
                pylab.plot(xyz[:,xcoord],xyz[:,ycoord],marker=marker,linestyle='',color='red',rasterized=rasterized)

        if(Npart_all[2]>0):
            xyz,mask=center_and_clip(load_fire_snap('Coordinates',2,sdir,snum),cen,rcut,numpart)
            if(xyz.size > 0):
                if not (quiet): 
                    m_dm+=np.sum(load_fire_snap('Masses',2,sdir,snum,particle_mask=mask))
                    print('DM mass=',m_dm)
                pylab.plot(xyz[:,xcoord],xyz[:,ycoord],marker=marker,linestyle='',color='blue',rasterized=rasterized)

        if('mass_check' in key):
            if(Npart_all[3]>0):
                xyz,mask=center_and_clip(load_fire_snap('Coordinates',3,sdir,snum),cen,rcut,numpart)
                print('Position of special mass = ',xyz+cen)
                if(xyz.size > 0):
                    print('Special mass=',np.sum(load_fire_snap('Masses',3,sdir,snum,particle_mask=mask)))
                    pylab.plot(xyz[:,xcoord],xyz[:,ycoord],marker='x',markersize=20,linestyle='',color='blue',rasterized=rasterized)


    if('star' in key and Npart_all[4] > 0):
        xyz,mask=center_and_clip(load_fire_snap('Coordinates',4,sdir,snum),cen,rcut,numpart)
        if(xyz.size > 0):
            pylab.plot(xyz[:,xcoord],xyz[:,ycoord],marker=marker,linestyle='',color='black',rasterized=rasterized)
            if('extra' in key): 
                pylab.plot(xyz[:,xcoord],xyz[:,ycoord],marker='.',linestyle='',color='black',rasterized=rasterized)
            if not (quiet): 
                m_star=load_fire_snap('Masses',4,sdir,snum,particle_mask=mask)
                m_star_tot=np.sum(m_star)
                print('Stellar mass=',m_star_tot)
                print('Stellar center in plot=',np.median(xyz,axis=0)+cen)

                if('mass_check' in key):
                    print('(Star) Mass Range == ',np.min(m_star),np.median(m_star),np.mean(m_star),np.max(m_star))
                    ok=np.where(m_star < 0.5*np.median(m_star))[0]
                    m00 = np.median(m_star)
                    m00 = np.max(m_star)
                    hi_mass = np.where(m_star > 5.*np.median(m_star))[0]
                    if(hi_mass.shape[0] > 0):
                        q = m_star[hi_mass]
                        q = (q[np.argsort(q)])[::-1]
                        print(q)
                    for f_m1,size,colorx in zip([0.01,0.05,0.3],[2.,3.,10.],['red','lime','cyan']):
                        print(' Mass threshold == ',f_m1*m00)
                        ok=np.where((m_star > f_m1*m00))[0]
                        if(ok.shape[0] > 0):
                            pylab.plot(xyz[ok,xcoord],xyz[ok,ycoord],marker='o',markersize=size,linestyle='',color=colorx,rasterized=rasterized)
                
                if(2==0):
                    r2s=np.sum(xyz*xyz,axis=1); r_s=np.sqrt(r2s); okt=np.where(r_s<10000.)
                    wt=m_star
                    wt=load_fire_snap('StellarLuminosity_B',4,sdir,snum,particle_mask=mask)
                    mzx=load_fire_snap('Metallicity',4,sdir,snum,particle_mask=mask)
                    zsolfe=1.38e-3;
                    print('mean   Z/H=',np.log10(np.sum(wt[okt]*mzx[okt,0])/np.sum(wt[okt]*0.014)))
                    print('L-mean Fe/H=',np.log10(np.sum(wt[okt]*mzx[okt,10])/np.sum(wt[okt]*zsolfe)))   
                    print('M-mean Fe/H=',np.log10(np.sum(m_star[okt]*mzx[okt,10])/np.sum(m_star[okt]*zsolfe)))   
                    print('med   Fe/H=',np.median(np.log10(mzx[okt,10]/zsolfe)))   
                
                wt=m_star/np.sum(m_star)
                tage=load_fire_snap('StellarAgeGyr',4,sdir,snum,particle_mask=mask)
                aform=load_fire_snap('StellarFormationTime',4,sdir,snum,particle_mask=mask)
                s=np.argsort(tage); tage_med=(tage[s])[(np.where(np.cumsum(wt[s]) > 0.5)[0])[0]]
                s=np.argsort(aform); aform_med=(aform[s])[(np.where(np.cumsum(wt[s]) > 0.5)[0])[0]]
                print('Median stellar formation scale factor a=',aform_med,' (',np.median(aform),') ',' median age of universe at form =',13.9-tage_med,' (',13.9-np.median(tage),')')
    if('bh' in key and Npart_all[5] > 0):
        xyz,mask=center_and_clip(load_fire_snap('Coordinates',5,sdir,snum),cen,rcut,numpart)
        if(xyz.size > 0):
            pylab.plot(xyz[:,xcoord],xyz[:,ycoord],marker='x',markersize=20,linestyle='',color='magenta',rasterized=rasterized)
            if not (quiet): 
                m_bh=load_fire_snap('Masses',5,sdir,snum,particle_mask=mask)
                m_bh_tot=np.sum(m_bh)
                print('Total BH particle mass=',np.sum(m_bh),' Number of SMBH in plot=',m_bh.size)
                #print('BH median in plot=',np.median(xyz,axis=0)+cen)
                print('BH com in plot=',np.sum((m_bh*np.transpose(xyz)).transpose(),axis=0)/m_bh_tot+cen)
                print('BH particles largest-to-smallest: ',np.log10(1.e10*m_bh[np.argsort(m_bh)][::-1]))
                m_bh_t=load_fire_snap('BH_Mass',5,sdir,snum,particle_mask=mask)
                if(np.array(m_bh_t).size>0):
                    if(np.max(m_bh_t)>0):
                        print('BH_Mass largest-to-smallest : ',np.log10(1.e10*m_bh_t[np.argsort(m_bh_t)][::-1]))
    if('gas' in key and Npart_all[0] > 0):
        xyz,mask=center_and_clip(load_fire_snap('Coordinates',0,sdir,snum),cen,rcut,numpart)
        if('temp' in key):
            t=load_fire_snap('Temperature',0,sdir,snum).take(mask,axis=0)
            if not (quiet): 
                m = load_fire_snap('Masses',0,sdir,snum,particle_mask=mask)
                m_gas=np.sum(m)
                print('Gas mass=',m_gas,' cell number = ',m.size)
            tcold = 0.5e4; thot = 1.e6;
            tcold = 8000.; thot = 3.e5;
            tcold = 8000.; thot = 1.e6;
            #tcold = 300.; thot = 5000.;
            ok=np.where(t > thot)[0]
            if(ok.shape[0] > 0):
                pylab.plot(xyz[ok,xcoord],xyz[ok,ycoord],marker=marker,linestyle='',color='orange',rasterized=rasterized)
                if not (quiet): 
                    m_hot = np.sum(m[ok])
                    print(' Gas mass [hot]=',m_hot)
                    e_hot_tot = np.sum(1.989e53*m[ok]*load_fire_snap('InternalEnergy',0,sdir,snum).take(mask,axis=0)[ok])
                    e_therm_tot = np.sum(1.989e53*m*load_fire_snap('InternalEnergy',0,sdir,snum).take(mask,axis=0))
                    print('   ( Hot energy = {:5.1e} erg (of {:5.1e}), or {:5.1e} SNe from {:5.1e} Msun stars )'.format(e_hot_tot,e_therm_tot,e_hot_tot/1.e51,e_hot_tot/1.e51*70.))
            ok=np.where((t > tcold)&(t < thot))[0]
            if(ok.shape[0] > 0):
                pylab.plot(xyz[ok,xcoord],xyz[ok,ycoord],marker=marker,linestyle='',color='lime',rasterized=rasterized)
                if not (quiet): 
                    m_warm = np.sum(m[ok])
                    print(' Gas mass [warm]=',m_warm)
            ok=np.where(t < tcold)[0]
            if(ok.shape[0] > 0):
                pylab.plot(xyz[ok,xcoord],xyz[ok,ycoord],marker=marker,linestyle='',color='purple',rasterized=rasterized)
                if not (quiet): 
                    m_cold = np.sum(m[ok])
                    print(' Gas mass [cold]=',m_cold)

            if('density_check' in key):
                nH=load_fire_snap('NumberDensity',0,sdir,snum).take(mask,axis=0)
                ok=np.where(nH > 6.e3)[0]
                if(ok.shape[0] > 0):
                    pylab.plot(xyz[ok,xcoord],xyz[ok,ycoord],marker='o',linestyle='',color='magenta',rasterized=rasterized)
                    if not (quiet): 
                        print(' Gas mass [super-dense]=',np.sum(m[ok]))


            if('mass_check' in key):
                print('(Gas) Mass Range == ',np.min(m),np.median(m),np.mean(m),np.max(m))
                ok=np.where(m < 0.5*np.median(m))[0]
                m00 = np.median(m)
                m00 = 0.3*np.max(m)
                for f_m1,size in zip([0.01,0.1,0.5],[2.4,0.8,0.2]):
                    print(' Mass threshold == ',f_m1*m00)
                    ok=np.where((t > thot)&(m < f_m1*m00))[0]
                    if(ok.shape[0] > 0):
                        pylab.plot(xyz[ok,xcoord],xyz[ok,ycoord],marker='o',markersize=size,linestyle='',color='orange',rasterized=rasterized)
                        if not (quiet): 
                            m_hot = np.sum(m[ok])
                            print(' -- Gas mass [hot]=',m_hot)
                    ok=np.where((t > tcold)&(t < thot)&(m < f_m1*m00))[0]
                    if(ok.shape[0] > 0):
                        pylab.plot(xyz[ok,xcoord],xyz[ok,ycoord],marker='o',markersize=size,linestyle='',color='lime',rasterized=rasterized)
                        if not (quiet): 
                            m_warm = np.sum(m[ok])
                            print(' -- Gas mass [warm]=',m_warm)
                    ok=np.where((t < tcold)&(m < f_m1*m00))[0]
                    if(ok.shape[0] > 0):
                        pylab.plot(xyz[ok,xcoord],xyz[ok,ycoord],marker='o',markersize=size,linestyle='',color='purple',rasterized=rasterized)
                        if not (quiet): 
                            m_cold = np.sum(m[ok])
                            print(' -- Gas mass [cold]=',m_cold)


            if('extra' in key):
                vxyz=load_fire_snap('Velocities',0,sdir,snum).take(mask,axis=0)
                ok=np.where(t > 1.e7)[0]
                if(ok.shape[0] > 0):
                    pylab.plot(xyz[ok,xcoord],xyz[ok,ycoord],marker='.',linestyle='',color='blue',rasterized=rasterized)
                    if not (quiet): 
                        m_hot = np.sum(m[ok])
                        print(' Gas mass [7.5-hot]=',m_hot,' (blue)')
                        print(' V_rms = ',np.std(vxyz[ok,0]),np.std(vxyz[ok,1]),np.std(vxyz[ok,2]))
                        print(' V_max = ',np.max(np.abs(vxyz[ok,0])),np.max(np.abs(vxyz[ok,1])),np.max(np.abs(vxyz[ok,2])))
                ok=np.where(t > 3.e7)[0]
                if(ok.shape[0] > 0):
                    pylab.plot(xyz[ok,xcoord],xyz[ok,ycoord],marker='.',linestyle='',color='gold',rasterized=rasterized)
                    if not (quiet): 
                        m_hot = np.sum(m[ok])
                        print(' Gas mass [7.5-hot]=',m_hot,' (gold)')
                        print(' V_rms = ',np.std(vxyz[ok,0]),np.std(vxyz[ok,1]),np.std(vxyz[ok,2]))
                        print(' V_max = ',np.max(np.abs(vxyz[ok,0])),np.max(np.abs(vxyz[ok,1])),np.max(np.abs(vxyz[ok,2])))
                ok=np.where(t > 1.e8)[0]
                if(ok.shape[0] > 0):
                    pylab.plot(xyz[ok,xcoord],xyz[ok,ycoord],marker='.',linestyle='',color='orange',rasterized=rasterized)
                    if not (quiet): 
                        m_hot = np.sum(m[ok])
                        print(' Gas mass [8.0-hot]=',m_hot,' (orange)')
                        print(' V_rms = ',np.std(vxyz[ok,0]),np.std(vxyz[ok,1]),np.std(vxyz[ok,2]))
                        print(' V_max = ',np.max(np.abs(vxyz[ok,0])),np.max(np.abs(vxyz[ok,1])),np.max(np.abs(vxyz[ok,2])))
                ok=np.where(t > 3.e8)[0]
                if(ok.shape[0] > 0):
                    pylab.plot(xyz[ok,xcoord],xyz[ok,ycoord],marker='.',linestyle='',color='red',rasterized=rasterized)
                    if not (quiet): 
                        m_hot = np.sum(m[ok])
                        print(' Gas mass [8.5-hot]=',m_hot,' (red)')
                        print(' V_rms = ',np.std(vxyz[ok,0]),np.std(vxyz[ok,1]),np.std(vxyz[ok,2]))
                        print(' V_max = ',np.max(np.abs(vxyz[ok,0])),np.max(np.abs(vxyz[ok,1])),np.max(np.abs(vxyz[ok,2])))
                ok=np.where(t > 9.e8)[0]
                if(ok.shape[0] > 0):
                    pylab.plot(xyz[ok,xcoord],xyz[ok,ycoord],marker='.',linestyle='',color='magenta',rasterized=rasterized)
                    if not (quiet): 
                        m_hot = np.sum(m[ok])
                        print(' Gas mass [9.0-hot]=',m_hot,' (magenta)')
                        print(' V_rms = ',np.std(vxyz[ok,0]),np.std(vxyz[ok,1]),np.std(vxyz[ok,2]))
                        print(' V_max = ',np.max(np.abs(vxyz[ok,0])),np.max(np.abs(vxyz[ok,1])),np.max(np.abs(vxyz[ok,2])))
                ok=np.where(t > 15.e8)[0]
                if(ok.shape[0] > 0):
                    pylab.plot(xyz[ok,xcoord],xyz[ok,ycoord],marker='.',linestyle='',color='violet',rasterized=rasterized)
                    if not (quiet): 
                        m_hot = np.sum(m[ok])
                        print(' Gas mass [9.2-hot]=',m_hot,' (violet)')
                        print(' V_rms = ',np.std(vxyz[ok,0]),np.std(vxyz[ok,1]),np.std(vxyz[ok,2]))
                        print(' V_max = ',np.max(np.abs(vxyz[ok,0])),np.max(np.abs(vxyz[ok,1])),np.max(np.abs(vxyz[ok,2])))

            if('bh' in key):
                bh_id_spec=True
                if(bh_id_spec):
                    ok=np.where(load_fire_snap('ParticleIDs',0,sdir,snum).take(mask,axis=0) == 1913298393)[0]
                    if(ok.shape[0] > 0):
                        pylab.plot(xyz[ok,xcoord],xyz[ok,ycoord],marker='.',linestyle='',color='magenta',rasterized=rasterized)
                        print(' Number of un-merged AGN jet cells=',xyz[ok,xcoord].size)
        else:
            pylab.plot(xyz[:,xcoord],xyz[:,ycoord],marker=marker,linestyle='',color='green',rasterized=rasterized)
            if not (quiet): 
                m_gas=np.sum(load_fire_snap('Masses',0,sdir,snum,particle_mask=mask))
                print('Gas mass=',m_gas)
    if not (quiet): 
        if(m_dm>0):
            m_star_tot = np.sum(m_star)
            print('Baryon fraction=',(m_bh_tot+m_gas+m_star_tot)/(m_bh_tot+m_gas+m_star_tot+m_dm) / (0.162))

    if(pdf): pylab.savefig('qlook_'+key+'.pdf',transparent=True)
    return



def quicklook_stellar_mass_function(sdir,snum,cen=np.zeros(0),rcut=100.,dlogm=0.1,**kwargs):
    '''
    subroutine to quickly estimate the mass function of star particles
    '''
    xyz,mask = center_and_clip(load_fire_snap('Coordinates',4,sdir,snum),cen,rcut,1e9)
    m = 1.e10 * load_fire_snap('Masses',4,sdir,snum).take(mask,axis=0)
    log_m = np.log10(m); m_min=np.min(log_m)-0.1; m_max=np.max(log_m)+0.1;
    bins = np.linspace(m_min,m_max,1+np.round((m_max-m_min)/dlogm).astype('int'))
    y,xb = np.histogram(log_m,bins=bins,weights=m)
    pylab.xlabel(r'Star Particle Mass $m_{i}$ [$M_{\odot}$]')
    pylab.axis([m_min,m_max,np.min(y[y>0])/1.5,np.max(y)*1.5])
    pylab.plot(xb[0:-1]+0.5*np.diff(xb),y/dlogm,linestyle='-',**kwargs)
    pylab.yscale('log')
    pylab.ylabel(r'Mass Function [$m_{i}\,dN_{i}/d\log{m_{i}}$]')
    return



def quicklook_sfr_time(sdir,snum,cen=np.zeros(0),rcut=100.,dt_Myr=100.,key='sfr'):
    '''
    subroutine to quickly estimate and plot the SF history
    '''
    xyz,mask = center_and_clip(load_fire_snap('Coordinates',4,sdir,snum),cen,rcut,1e9)
    age = load_fire_snap('StellarAgeGyr',4,sdir,snum).take(mask,axis=0)
    print('Median age [Gyr] == ',np.median(age))
    m = load_fire_snap('Masses',4,sdir,snum).take(mask,axis=0)
    bins = np.linspace(0.,13.8,np.round(13.8*1000./dt_Myr).astype('int'))
    y,xb = np.histogram(age,bins=bins,weights=m)
    y *= 1.e10 / (dt_Myr*1.e6)
    pylab.xlabel(r'Lookback Time  [Gyr]')
    pylab.axis([0.,13.8,np.min(y[y>0])/1.5,np.max(y)*1.5])
    if('z' in key):
        aform = load_fire_snap('StellarFormationTime',4,sdir,snum).take(mask,axis=0)
        print('Median formation redshift == ',1./np.median(aform)-1.)
        s=np.argsort(age); age_s=age[s]; aform_s=aform[s]; 
        x_a = np.interp(xb,age_s,aform_s)
        x_z = 1./x_a-1.
        xb = x_z; pylab.xlabel(r'Redshift z'); pylab.axis([0.,10.,np.min(y[y>0])/1.5,np.max(y)*1.5])
        xb = x_a; pylab.xlabel(r'Scale Factor $a=1/(1+z)$'); pylab.axis([1.,0.,np.min(y[y>0])/1.5,np.max(y)*1.5])
    pylab.plot(xb[0:-1]+0.5*np.diff(xb),y,linestyle='-')
    pylab.yscale('log')
    pylab.ylabel(r'Archeological SFR  [${\rm M}_{\odot}$/yr]')
    return



def quicklook_phase(sdir,snum,cen=np.zeros(0),rcut=100.,marker=',',key='phase'):
    '''
    subroutine to plot temperature-density diagram for gas in snapshot
    '''
    if('simple' in key):
        nH = 406.*load_from_snapshot('Densities',0,sdir,snum)
        T = 80.8*load_from_snapshot('InternalEnergy',0,sdir,snum)
        ok = np.where(rho > 1.e-2)
        pylab.plot(np.log10(nH[ok]),np.log10(T[ok]),marker=marker,linestyle='',color='black',rasterized=True);
        pylab.xlabel(r'Gas Density $\log_{10}(n_{n})$  [${\rm cm^{-3}}$]')
        pylab.ylabel(r'Gas Temperature $\log_{10}(T)$  [K]')
        return
    
    xyz,mask = center_and_clip(load_fire_snap('Coordinates',0,sdir,snum),cen,rcut,1e9)
    #nH = np.log10( 406.4 * load_fire_snap('Density',0,sdir,snum).take(mask,axis=0) )
    #T = np.log10( 47.65 * load_fire_snap('InternalEnergy',0,sdir,snum).take(mask,axis=0) )
    nH = load_fire_snap('NumberDensity',0,sdir,snum).take(mask,axis=0) 
    T = load_fire_snap('Temperature',0,sdir,snum).take(mask,axis=0) 
    
    pylab.plot(np.log10(nH),np.log10(T),marker=marker,linestyle='',color='black',rasterized=True);
    pylab.xlabel(r'Gas Density $\log_{10}(n_{n})$  [${\rm cm^{-3}}$]')
    pylab.ylabel(r'Gas Temperature $\log_{10}(T)$  [K]')
    
    if('all' in key):
        Ecr_N = load_fire_snap('CosmicRayEnergy',0,sdir,snum).take(mask,axis=0) 
        Erad_N = load_fire_snap('PhotonEnergy',0,sdir,snum).take(mask,axis=0) 
        Ekin_N = load_fire_snap('Velocities',0,sdir,snum).take(mask,axis=0) 
        EB_N = load_fire_snap('MagneticField',0,sdir,snum).take(mask,axis=0) 
        m = load_fire_snap('Masses',0,sdir,snum).take(mask,axis=0) 

        T_Ecr = np.sum(Ecr_N, axis=1) / m * 40.4
        pylab.plot(np.log10(nH),np.log10(T_Ecr),marker=marker,linestyle='',color='blue',rasterized=True);

        T_Erad = np.sum(Erad_N, axis=1) / m * 40.4
        pylab.plot(np.log10(nH),np.log10(T_Erad),marker=marker,linestyle='',color='lime',rasterized=True);

        T_Ekin = np.sum(Ekin_N*Ekin_N, axis=1) * 60.6
        pylab.plot(np.log10(nH),np.log10(T_Ekin),marker=marker,linestyle='',color='gold',rasterized=True);
        
        T_EB = 2.88e14 * np.sum(EB_N*EB_N, axis=1) / nH
        pylab.plot(np.log10(nH),np.log10(T_EB),marker=marker,linestyle='',color='red',rasterized=True);
        
        pylab.plot(np.log10(nH),np.log10(T),marker=marker,linestyle='',color='black',rasterized=True);
        
        ok = np.where((np.log10(T) > 5.5) & (np.log10(T) > 5.8+2.*(np.log10(nH)-(-4.))))
        print('Mass in problem region = ',np.log10(np.sum(m[ok]))+10.)
        pylab.plot(np.log10(nH[ok]),np.log10(T[ok]),marker='.',markersize=1,linestyle='',color='purple',rasterized=True);
        
        pylab.axis([-6.,0.,0.,9.5])
    
    return
    


def quicklook_b_vs_rho(sdir,snum,cen=np.zeros(0),rcut=100.):
    '''
    subroutine to plot magnetic energy density vs density diagram for gas in snapshot
    '''
    xyz,mask = center_and_clip(load_fire_snap('Coordinates',0,sdir,snum),cen,rcut,1e9)
    #nH = np.log10( 406.4 * load_fire_snap('Density',0,sdir,snum).take(mask,axis=0) )
    #T = np.log10( 47.65 * load_fire_snap('InternalEnergy',0,sdir,snum).take(mask,axis=0) )
    nH = np.log10( load_fire_snap('NumberDensity',0,sdir,snum).take(mask,axis=0) )
    B = np.log10( 1.e6 * load_fire_snap('Bmag',0,sdir,snum).take(mask,axis=0) )
    
    pylab.plot(nH,B,marker=',',linestyle='',color='black',rasterized=True);
    pylab.xlabel(r'Gas Density $\log_{10}(n_{n})$  [${\rm cm^{-3}}$]')
    pylab.ylabel(r'Magnetic Field Strength $|{\bf B}|$  [$\mu {\rm G}$]')
    #pylab.ylabel(r'Magnetic Energy Density $|{\bf B}|^{2}/8\pi$  [${\rm eV\,cm^{-3}}$]')
    return



def quicklook_vc_profile(sdir,snum,cen=np.zeros(0),rcut=100.,key='vc'):
    '''
    subroutine to quickly estimate and plot the circular velocity curve
    '''
    r2_all=np.zeros(0); m_all=np.zeros(0);
    ptype_set=[0,1,4]
    ptype_set=[0,1,2,3,4,5]
    npart_total = load_fire_snap('NumPart_Total',-1,sdir,snum)
    
    for ptype in ptype_set:
        if(npart_total[ptype]>0):
            xyz,mask = center_and_clip(load_fire_snap('Coordinates',ptype,sdir,snum),cen,rcut,1e9)
            m = load_fire_snap('Masses',ptype,sdir,snum).take(mask,axis=0)
            r2 = np.sum(xyz*xyz,axis=1)
            m_all = np.concatenate([m_all,m],axis=0)
            r2_all = np.concatenate([r2_all,r2],axis=0)
    s=np.argsort(r2_all);
    r2=r2_all[s]; mt=np.cumsum(m_all[s]); r=np.sqrt(r2); vc=np.sqrt(6.67e-8*mt*1.e10*2.e33/((r+1.e-3*rcut)*3.086e21))/1.e5;
    ymax = np.max(vc)*1.05
    ylabel = r'Velocity  [km/s]'
    if('acc' in key):
        acc = vc*vc/r
        accnorm = 40541.6; # = pi*G*Sigma_crit / (kms^2/kpc), for Sigma_crit~3000 Msun/pc^2
        acc /= accnorm
        pylab.plot(r,acc,linestyle='-',color='black',label=r'$\bar{a}_{\rm grav}$')
        pylab.yscale('log'); pylab.xscale('log')
        pylab.axis([np.min(r),rcut,0.9*np.min(acc),1.1*np.max(acc)])
        ylabel = r'Acceleration / $\langle \dot{p}/m_{\ast} \rangle$'
    else:
        pylab.plot(r,vc,linestyle='-',color='black',label=r'$V_{c}$')
        pylab.axis([0.,rcut,0.,ymax])

    if('sig' in key or 'disp' in key):
        xyz,mask = center_and_clip(load_fire_snap('Coordinates',4,sdir,snum),cen,rcut,1e9)
        m = load_fire_snap('Masses',4,sdir,snum).take(mask,axis=0)
        v = (load_fire_snap('Velocities',4,sdir,snum).take(mask,axis=0)).transpose(); vx=v[0]; vy=v[1]; vz=v[2]; 
        vx-=np.median(vx); vy-=np.median(vy); vz-=np.median(vz); 
        r2 = np.sum(xyz*xyz,axis=1)
        s=np.argsort(r2);
        r2=r2[s]; m=m[s]; r=np.sqrt(r2); vx=vx[s]; vy=vy[s]; vz=vz[s]; mt=np.cumsum(m)
        vxt=np.cumsum(m*vx)/mt; vyt=np.cumsum(m*vy)/mt; vzt=np.cumsum(m*vz)/mt; 
        vxt2=np.cumsum(m*vx*vx)/mt-vxt*vxt; vyt2=np.cumsum(m*vy*vy)/mt-vyt*vyt; vzt2=np.cumsum(m*vz*vz)/mt-vzt*vzt; 
        dvx=np.sqrt(vxt2); dvy=np.sqrt(vyt2); dvz=np.sqrt(vzt2); 
    
        pylab.plot(r,dvx,linestyle='-',color='red',label=r'$\langle\sigma_{x}(<r)\rangle$ (sphere)')
        pylab.plot(r,dvy,linestyle='-',color='blue',label=r'$\langle\sigma_{y}(<r)\rangle$ (sphere)')
        pylab.plot(r,dvz,linestyle='-',color='green',label=r'$\langle\sigma_{z}(<r)\rangle$ (sphere)')

        xyz,mask = center_and_clip(load_fire_snap('Coordinates',4,sdir,snum),cen,rcut,1e9)
        m = load_fire_snap('Masses',4,sdir,snum).take(mask,axis=0)
        v = (load_fire_snap('Velocities',4,sdir,snum).take(mask,axis=0)).transpose(); vx=v[0]; vy=v[1]; vz=v[2]; 
        vx-=np.median(vx); vy-=np.median(vy); vz-=np.median(vz); xyz=xyz.transpose(); x=xyz[0]; y=xyz[1]; z=xyz[2]; 
        r2xy=x*x+y*y; r2xz=x*x+z*z; r2yz=y*y+z*z; 
        for r2,v0,color,label in zip([r2yz,r2xz,r2xy],[vx,vy,vz],['red','blue','green'],[r'$\sigma_{x}(R)$ (proj)',r'$\sigma_{y}(R)$ (proj)',r'$\sigma_{z}(R)$ (proj)']):
            dlogr=0.1; logr=0.5*np.log10(r2); rbin=np.arange(0.5*np.log10(np.min(r2)),0.5*np.log10(np.max(r2)),dlogr); u = 0.*rbin
            for j,logr_0 in zip(range(rbin.size),rbin):
                ok=np.where((logr>=logr_0-0.5*dlogr) & (logr<=logr_0+0.5*dlogr))[0]; n0=logr[ok].size
                if(n0 > 3):
                    v0t=v0[ok]; s=np.argsort(v0t); v0ss=v0t[s]; u[j]=0.74*(v0ss[np.round(0.75*n0).astype('int')]-v0ss[np.round(0.25*n0).astype('int')])
            pylab.plot(10.**rbin,u,linestyle=':',linewidth=3.,color=color,label=label)
            r0 = np.sqrt(np.median(r2))
        
            ok=np.where((r2 < (1.0*r0)**2))[0]; v0t=v0[ok]; s=np.argsort(v0t); v0ss=v0t[s]; n0=v0ss.size; dv00=0.74*(v0ss[np.round(0.75*n0).astype('int')]-v0ss[np.round(0.25*n0).astype('int')])
            pylab.plot(10.**rbin,0.*rbin+dv00,linestyle=':',linewidth=1.,color=color)
            pylab.plot(np.array([r0,r0]),np.array([-1.,1.e4]),linestyle=':',linewidth=1.,color=color)

    r_eff = np.median(r)
    pylab.plot(r_eff*np.array([1.,1.]),np.array([-0.1*ymax,ymax*1.1]),linestyle=':',color='black')
    pylab.plot(0.5*r_eff*np.array([1.,1.]),np.array([-0.1*ymax,ymax*1.1]),linestyle=':',color='purple')
    pylab.plot(0.125*r_eff*np.array([1.,1.]),np.array([-0.1*ymax,ymax*1.1]),linestyle=':',color='violet')
    pylab.legend(loc='upper right',handletextpad=0.5,borderpad=0.5,numpoints=1,
            frameon=False,columnspacing=0.5,labelspacing=0.5)
    pylab.xlabel(r'Galactocentric Radius $r$  [kpc]')
    pylab.ylabel(ylabel)

    return
    



def beta_of_n_sersic(n):
    # beta is beta**1/n, which is much better behaved: get excellent 
    #   fit to the full numerical solution (to <1% for 0.1<n_s<20) with:
    d0=0.163382; b0=1.9994830; c0=-0.32670002; n0=4.;
    b=3.*n*n*np.exp(-(n/(1.5*d0))**n0) + (c0+b0*n)/(1.+10.*np.exp(-(n/d0)**n0)/((n/0.1)**n0))
    return b;

def sersic(ln_r, ln_I_e, ln_r_e, n):
    r = np.exp(ln_r)
    r_e = np.exp(ln_r_e)
    b_n = beta_of_n_sersic(n);
    return ln_I_e - b_n * ( (r/r_e)**(1./n) - 1.);
    
def bulge_disk(ln_r, ln_Ibul, ln_rbul, nbul, ln_Idisk, dln_rdisk):
    return np.log( np.exp(sersic(ln_r, ln_Ibul,ln_rbul,nbul)) + 
                    np.exp(sersic(ln_r, ln_Idisk,ln_rbul+dln_rdisk,1.))  )

def calc_BT_fit(ln_Ibul, ln_rbul, nbul, ln_Idisk, dln_rdisk):
    dx=0.01; x=np.arange(ln_rbul-10.,ln_rbul+dln_rdisk+10.,dx)
    r=np.exp(x); A=np.pi*r*r; dA=0.*A; dA[1::]=A[1::]-A[0:-1];
    ybulge=np.exp(sersic(x,ln_Ibul,ln_rbul,nbul))
    ydisk =np.exp(sersic(x,ln_Idisk,ln_rbul+dln_rdisk,1.))
    mbulge=np.sum(ybulge*dA)
    mdisk =np.sum(ydisk *dA)
    mtot = mbulge + mdisk
    return mbulge / mtot

def quicklook_light_profile(sdir,snum,cen=np.zeros(0),rcut=100.,
    key='lightprofile',weight='B'):
    '''
    subroutine to quickly estimate and plot the projected [face-on] light profile
    '''
    ptype=4;
    weight='StellarLuminosity_'+weight
    if(('mass' in weight) or ('Mass' in weight) or (weight=='m')): weight='Masses';

    xyz,mask = center_and_clip(load_fire_snap('Coordinates',ptype,sdir,snum),cen,rcut,1e9) # load positions
    m = load_fire_snap('Masses',ptype,sdir,snum).take(mask,axis=0) # load masses [or weights, more generically]
    vxyz = load_fire_snap('Velocities',ptype,sdir,snum).take(mask,axis=0) # load velocities
    vxyz = vxyz - np.median(vxyz,axis=0) # reset to median local velocity about r=0
    v2_mag = np.sum(vxyz*vxyz,axis=1);
    jvec=np.cross(vxyz,xyz); m_jvec=(m*(jvec.transpose())).transpose(); # compute j vector
    j_tot=np.sum(m_jvec,axis=0); j_hat=j_tot/np.sqrt(np.sum(j_tot*j_tot)); # compute angular momentum vectors
    xyz=xyz.transpose(); z=xyz[0]*j_hat[0]+xyz[1]*j_hat[1]+xyz[2]*j_hat[2]; # project onto J
    r2 = xyz[0]*xyz[0]+xyz[1]*xyz[1]+xyz[2]*xyz[2]-z*z; wt = 1.e10*m; # compute positions and weights
    if(weight != 'Masses'): 
        wt=load_fire_snap(weight,ptype,sdir,snum).take(mask,axis=0) # actually compute weights to use below for distribution function

    rmax=np.max(r2); 
    if(rcut<rmax): rmax=rcut;
    log_r = 0.5*np.log(r2)
    log_rg = np.linspace(np.min(log_r),np.log(rmax),100)
    y,xb=np.histogram(log_r,bins=log_rg,weights=wt)
    r0 = np.exp(xb[0:-1]+0.5*np.diff(xb))
    y *= 1. / ((xb[1]-xb[0]) * 2.*np.pi*r0*r0)
    sig = 1./np.sqrt(y * r0*r0)

    lin_r = np.sqrt(r2)
    dx_res = 0.5
    lin_rg = np.arange(0.,rmax,dx_res)
    y_lin,xb_lin=np.histogram(lin_r,bins=lin_rg,weights=wt)
    r0_lin = xb_lin[0:-1]+0.5*np.diff(xb_lin)
    y_lin *= 1. / ((xb_lin[1]-xb_lin[0]) * 2.*np.pi*r0_lin)
    sig_lin = 1./np.sqrt(y_lin * r0_lin)
    rhalf=np.median(np.sqrt(r2)); 
    ieff=0.5*np.sum(wt)/(4.*np.pi*rhalf*rhalf);

    s=np.argsort(lin_r); wt_s=wt[s]; q=np.cumsum(wt_s); q/=np.max(q); rhalf_wt=np.min((lin_r[s])[(q>0.5)])

    pylab.xscale('log');   
    #pylab.plot(r0,np.log10(y),linestyle='-',color='blue',label='log-sampled')
    pylab.plot(r0,np.log10(y),linestyle='-',color='lime',label=r'Sim: (particle $R_e$={:.2f}, light $R_e$={:.2f})'.format(rhalf,rhalf_wt))
    #pylab.plot(r0_lin,np.log10(y_lin),linestyle='-.',color='red',label='lin-sampled')
    
    
    ok=np.where((r0>0.05)&(r0<20)&(r0>rhalf/10.)&(r0<rhalf*5.)&(y>0)&(y>ieff/10000.)&(np.isfinite(y)))[0]
    #pylab.plot(r0[ok],np.log10(y[ok]),linestyle='--',color='skyblue',linewidth=2,label='trimmed: log-sampled')

    ok_lin=np.where((r0_lin>0.05)&(r0_lin<20)&(r0_lin>rhalf/10.)&(r0_lin<rhalf*5.)&(y_lin>0)&(y_lin>ieff/10000.)&(np.isfinite(y_lin)))[0]
    #pylab.plot(r0_lin[ok_lin],np.log10(y_lin[ok_lin]),linestyle='--',color='pink',linewidth=2,label='trimmed: lin-sampled')

    
    x0=np.log(r0[ok]); y0=np.log(y[ok]); s0=sig[ok]
    pguess=np.array([np.log(ieff), np.log(rhalf), 1.])
    lobound_1s = [np.log(np.min(y[ok]))-8. , np.log(rhalf)-8., 0.25]
    hibound_1s = [np.log(np.max(y[ok]))+8. , np.log(rhalf)+8., 8.00]
    #print('low = ',np.log10(np.exp(lobound_1s[0])),np.exp(lobound_1s[1]),lobound_1s[2])
    #print('hig = ',np.log10(np.exp(hibound_1s[0])),np.exp(hibound_1s[1]),hibound_1s[2])
    popt, pcov = curve_fit(sersic, x0, y0, sigma=s0, bounds=(lobound_1s,hibound_1s), p0=pguess, absolute_sigma=False)
    print('guess                == ',np.log10(np.exp(pguess[0])),np.exp(pguess[1]),pguess[2])
    print('params [log; purple] == ',np.log10(np.exp(popt[0])),np.exp(popt[1]),popt[2])
    #pylab.plot(np.exp(x0), sersic(x0, *popt) /np.log(10.), linestyle=':',color='purple',label='1-Sersic fit: log-sampled')

    x0_lin=np.log(r0_lin[ok_lin]); y0_lin=np.log(y_lin[ok_lin]); s0_lin=sig_lin[ok_lin]
    popt, pcov = curve_fit(sersic, x0_lin, y0_lin, sigma=s0_lin, bounds=(lobound_1s,hibound_1s), p0=pguess, absolute_sigma=False)
    print('params [lin; orange] == ',np.log10(np.exp(popt[0])),np.exp(popt[1]),popt[2])
    #pylab.plot(np.exp(x0_lin), sersic(x0_lin, *popt) /np.log(10.), linestyle=':',color='orange',label='1-Sersic fit: lin-sampled')


    pguess_2s=np.array([np.log(ieff)+3., np.log(rhalf)-2., 4., np.log(ieff)-1., 2.])
    lobound_2s = [np.log(np.min(y[ok]))-2. , np.log(rhalf)-8., 0.25, np.log(np.min(y[ok]))-8., 0.]
    hibound_2s = [np.log(np.max(y[ok]))+8. , np.log(rhalf)+2., 8.00, np.log(np.max(y[ok]))+2., 8.]
    popt, pcov = curve_fit(bulge_disk, x0, y0, sigma=s0, bounds=(lobound_2s,hibound_2s), p0=pguess_2s, absolute_sigma=False)
    print('param2 [log; lime  ] == ',np.log10(np.exp(popt[0])),np.exp(popt[1]),popt[2],np.log10(np.exp(popt[3])),np.exp(popt[1]+popt[4]))
    #pylab.plot(np.exp(x0),  bulge_disk(x0, *popt) /np.log(10.), linestyle=':',color='lime',label='2-Sersic fit: log-sampled')
    print(' ->fit D/T [log] = ',1.-calc_BT_fit(*popt))
    popt_0=popt; DT_0=1.-calc_BT_fit(*popt);
    popt, pcov = curve_fit(bulge_disk, x0_lin, y0_lin, sigma=s0_lin, bounds=(lobound_2s,hibound_2s), p0=pguess_2s, absolute_sigma=False)
    print('param2 [lin; gold  ] == ',np.log10(np.exp(popt[0])),np.exp(popt[1]),popt[2],np.log10(np.exp(popt[3])),np.exp(popt[1]+popt[4]))
    #pylab.plot(np.exp(x0_lin),  bulge_disk(x0_lin, *popt) /np.log(10.), linestyle=':',color='gold',label='2-Sersic fit: lin-sampled')
    print(' ->fit D/T [lin] = ',1.-calc_BT_fit(*popt)); DT=1.-calc_BT_fit(*popt);
    if(DT_0>DT): 
        popt=popt_0; DT=DT_0;
    pylab.plot(np.exp(x0),  bulge_disk(x0, *popt) /np.log(10.), linestyle=':',color='black',linewidth=2.,label=r'2-Sersic fit: D/T={:.2f}'.format(DT))
    pylab.plot(np.exp(x0),  sersic(x0, popt[0],popt[1],popt[2]) /np.log(10.), linestyle=':',color='red',linewidth=2.,label=r'2-Sersic fit: Bulge [$R_e$={:.2f}, $n_s$={:.2f}]'.format(np.exp(popt[1]),popt[2]))
    pylab.plot(np.exp(x0),  sersic(x0, popt[3],popt[1]+popt[4],1.) /np.log(10.), linestyle=':',color='blue',linewidth=2.,label=r'2-Sersic fit: Disk [$R_e$={:.2f}, $n_s$=1]'.format(np.exp(popt[1]+popt[4])))

    pylab.axis([0.5*np.min(r0[ok]),1.2*np.max(r0[ok]),-0.3+np.min(np.log10(y[ok])),0.3+np.max(np.log10(y[ok]))])
    pylab.xscale('linear')
    pylab.legend(loc='upper right',handletextpad=0.5,borderpad=0.5,numpoints=1,
            frameon=False,columnspacing=0.5,labelspacing=0.5)
    pylab.xlabel(r'Galactocentric Projected Cylindrical Radius $R$  [kpc]')
    pylab.ylabel(r'$\log_{10}$[Projected Surface Brightness]  [${\rm L}_{\odot}/{\rm kpc^{2}}$]')

    return






def quicklook_mass_profile(sdir,snum,cen=np.zeros(0),rcut=100.,key='massprofile_dm',n_bins=100):
    '''
    subroutine to quickly estimate and plot the spherically-averaged mass profile
    '''
    ptypes=np.zeros(0).astype('int')
    surfdenkey=False
    if('2d' in key or '2D' in key or 'surf' in key): surfdenkey=True
    if('gas' in key): ptypes=np.append(ptypes,0)
    if('dm' in key): ptypes=np.append(ptypes,1)
    if('star' in key): ptypes=np.append(ptypes,4)
    if('bh' in key): ptypes=np.append(ptypes,5)
    for ptype in ptypes:
        xyz,mask = center_and_clip(load_fire_snap('Coordinates',ptype,sdir,snum),cen,rcut,1e9)
        m = load_fire_snap('Masses',ptype,sdir,snum).take(mask,axis=0)
        if(surfdenkey):
            r2=xyz[:,0]*xyz[:,0]+xyz[:,1]*xyz[:,1]
            if('eff' in key): r2 = np.sum(xyz*xyz,axis=1)
        else:
            r2 = np.sum(xyz*xyz,axis=1)
        rmax=2.0*np.max(np.sqrt(r2)); 
        rmin=0.5*np.min(np.sqrt(r2));
        if(rcut<rmax): rmax=rcut;
        if(rmin>=rmax): rmin=0.5*rmax
        log_r = 0.5*np.log(r2)
        log_rg = np.linspace(np.log(rmin),np.log(rmax),n_bins)
        if(ptype==5): log_rg = np.linspace(np.log(rmin),np.log(rmax),n_bins/5)
        y,xb=np.histogram(log_r,bins=log_rg,weights=m)
        r0 = np.exp(xb[0:-1]+0.5*np.diff(xb))
        if(surfdenkey):
            y *= 1.e10 / ((xb[1]-xb[0]) * 2.*np.pi*r0*r0)
            if('eff' in key): y *= 1.e-6;
        else:
            y *= 1.e10 / ((xb[1]-xb[0]) * 4.*np.pi*r0*r0*r0)
        pylab.plot(r0,y,linestyle='-',label=r'PType = '+str(ptype))
    pylab.yscale('log'); pylab.xscale('log')   
    pylab.legend(loc='upper right',handletextpad=0.5,borderpad=0.5,numpoints=1,
            frameon=False,columnspacing=0.5,labelspacing=0.5)
    if(surfdenkey):
        pylab.xlabel(r'Galactocentric Projected Cylindrical Radius $R$  [kpc]')
        pylab.ylabel(r'2D Mass Surface Density  [${\rm M}_{\odot}/{\rm kpc^{2}}$]')
        if('eff' in key): pylab.ylabel(r'2D Mass Surface Density  [${\rm M}_{\odot}/{\rm pc^{2}}$]')
    else:
        pylab.xlabel(r'Galactocentric Spherical Radius $r$  [kpc]')
        pylab.ylabel(r'3D Mass Density  [${\rm M}_{\odot}/{\rm kpc^{3}}$]')
    return



def quicklook_temp_profile(sdir,snum,cen=np.zeros(0),rcut=100.,key='t_profile'):
    '''
    subroutine to quickly estimate and plot the spherically-averaged temperature/entropy profiles
    '''
    ptype=0
    xyz,mask = center_and_clip(load_fire_snap('Coordinates',ptype,sdir,snum),cen,rcut,1e9)
    m = load_fire_snap('Masses',ptype,sdir,snum).take(mask,axis=0)
    n = load_fire_snap('Density',ptype,sdir,snum).take(mask,axis=0) * 404.6
    T = load_fire_snap('Temperature',ptype,sdir,snum).take(mask,axis=0)
    s = T / (n**(2./3.))
    r2 = np.sum(xyz*xyz,axis=1)
    rmax=np.max(r2); 
    if(rcut<rmax): rmax=rcut;
    log_r = 0.5*np.log(r2)
    log_rg = np.linspace(np.min(log_r),np.log(rmax),50)
    wt_y,xb=np.histogram(log_r,bins=log_rg,weights=m)
    if('log' in key):
        T=np.log10(T); n=np.log10(n); s=np.log10(s)
    sum_y_T,xb=np.histogram(log_r,bins=log_rg,weights=m * T )
    sum_y_n,xb=np.histogram(log_r,bins=log_rg,weights=m * n )
    sum_y_s,xb=np.histogram(log_r,bins=log_rg,weights=m * s )
    y_T = sum_y_T / wt_y
    y_n = sum_y_n / wt_y
    y_s = sum_y_s / wt_y
    if('log' in key):
        y_T=10.**y_T; y_n=10.**y_n; y_s=10.**y_s

    r0 = np.exp(xb[0:-1]+0.5*np.diff(xb))
    pylab.plot(r0,np.log10(y_T),linestyle='-',label=r'$T$ [${\rm K}$]')
    pylab.plot(r0,np.log10(y_n),linestyle='-',label=r'$n$ [${\rm cm^{-3}}$]')
    pylab.plot(r0,np.log10(y_s),linestyle='-',label=r'$s$ [${\rm K\,cm^{2}}$]')

    pylab.yscale('linear'); pylab.xscale('log')   
    pylab.legend(loc='lower left',handletextpad=0.5,borderpad=0.5,numpoints=1,
            frameon=False,columnspacing=0.5,labelspacing=0.5)
    pylab.xlabel(r'Galactocentric Radius $r$  [kpc]')
    pylab.ylabel(r'$\log_{10}( \langle$ Mass-Weighted Mean Value $\rangle )$')
    return

    

def quicklook_j_distrib(sdir,snum,cen=np.zeros(0),rcut=100.,key='gas_angmom',
    weight='B',plot=True):
    '''
    subroutine to quickly estimate and plot the distribution of angular momentum
    '''
    ptype=0;
    if('gas' in key): 
        ptype=0; 
        if(weight=='B'): weight='Masses';
    if('dm' in key): 
        ptype=1; weight='Masses';
    if('star' in key): 
        ptype=4;
        weight='StellarLuminosity_'+weight
    if(('mass' in weight) or ('Mass' in weight) or (weight=='m')): weight='Masses';
    print('Weight == ',weight)
    xyz,mask = center_and_clip(load_fire_snap('Coordinates',ptype,sdir,snum),cen,rcut,1e9) # load positions
    m = load_fire_snap('Masses',ptype,sdir,snum).take(mask,axis=0) # load masses [or weights, more generically]
    vxyz = load_fire_snap('Velocities',ptype,sdir,snum).take(mask,axis=0) # load velocities
    vxyz = vxyz - np.median(vxyz,axis=0) # reset to median local velocity about r=0
    v2_mag = np.sum(vxyz*vxyz,axis=1);
    jvec=np.cross(vxyz,xyz); m_jvec=(m*(jvec.transpose())).transpose(); # compute j vector
    j_tot=np.sum(m_jvec,axis=0); j_hat=j_tot/np.sqrt(np.sum(j_tot*j_tot)); j_z=np.sum(j_hat*jvec,axis=1); # compute total and project on it
    r20 = np.sum(xyz*xyz,axis=1);  m0=m; # get positions for interpolation below
    if(weight != 'Masses'): 
        if(weight=='NeutralHydrogenAbundance' or weight=='Z' or weight=='Temperature'):
            m0=m*load_fire_snap(weight,ptype,sdir,snum).take(mask,axis=0) # actually compute weights to use below for distribution function        
        else:
            m0=load_fire_snap(weight,ptype,sdir,snum).take(mask,axis=0) # actually compute weights to use below for distribution function

    
    r2_all=r20; m_all=m; # now compute the j for a circular orbit
    for ptype_t in [0,1,4]:
        if(ptype_t!=ptype):
            xyz,mask = center_and_clip(load_fire_snap('Coordinates',ptype_t,sdir,snum),cen,rcut,1e9)
            m = load_fire_snap('Masses',ptype_t,sdir,snum).take(mask,axis=0)
            r2 = np.sum(xyz*xyz,axis=1)
            m_all = np.concatenate([m_all,m],axis=0)
            r2_all = np.concatenate([r2_all,r2],axis=0)
    r_all=np.sqrt(r2_all); s=np.argsort(r_all); r=r_all[s]; m=m_all[s]; mr=m/r; r2=r*r;
    unit_vc2 = (6.67e-8 * 1.989e43 / 3.086e21) / 1.e10 # converts to (km/s)^2
    Gmr_inner=unit_vc2*np.cumsum(m)/r; Gmr_outer=unit_vc2*np.cumsum(mr[::-1])[::-1];
    vcirc_r = np.sqrt(Gmr_inner)
    phi_r = -(Gmr_inner + Gmr_outer)
    ecirc_r = phi_r + 0.5*vcirc_r*vcirc_r
    jcirc_r = r * vcirc_r
    jc_interp_element = np.interp(r20,r2,jcirc_r)
    phi_element = np.interp(r20,r2,phi_r)
    e_element = phi_element + 0.5*v2_mag
    s=np.argsort(ecirc_r)
    r2_of_e = np.interp(e_element,ecirc_r[s],r2[s])
    jc_of_e = np.interp(r2_of_e,r2,jcirc_r)
    jz_i_r = j_z / jc_interp_element
    jz_i_e = j_z / jc_of_e
    nbins=100
    if(np.median(jz_i_e) < 0.5): nbins=50
    bins = np.linspace(-1.05,1.15,nbins)
    
    eps_0=0.7;
    print('Fraction with circularity > 0.7:',np.sum(m0[(jz_i_r>=eps_0)])/np.sum(m0),np.sum(m0[(jz_i_e>=eps_0)])/np.sum(m0))
    
    y_r,xb = np.histogram(jz_i_r,bins=bins,weights=m0,density=True)
    y_e,xb = np.histogram(jz_i_e,bins=bins,weights=m0,density=True)
    x = xb[0:-1]+0.5*np.diff(xb)
    if(plot): 
        #pylab.plot(x,y_r,linestyle='-',color='pink',label=r'$j_{z}/j_{\rm circ}(r)$'+' (ptype='+str(ptype)+', weight='+weight.replace('_','\_')+')')# (ptype='+str(ptype)+', weight='+weight+')')
        pylab.plot(x,y_e,linestyle='-',color='black',label=r'$j_{z}/j_{\rm circ}(E)$'+' (ptype='+str(ptype)+', weight='+weight.replace('_','\_')+')')# (ptype='+str(ptype)+', weight='+weight+')')
        pylab.plot([0.,0.],[0.,np.max(y_e)*1.05],color='black',linestyle=':')
        pylab.plot([1.,1.],[0.,np.max(y_e)*1.05],color='black',linestyle=':')
        pylab.axis([-1.2,1.2,0.,np.max(y_e)*1.05])
    
        pylab.legend(loc='upper right',handletextpad=0.5,borderpad=0.5,numpoints=1,
                frameon=False,columnspacing=0.5,labelspacing=0.5)
        pylab.ylabel(r'Fraction [${\rm d}P/{\rm d}j$]')
        pylab.xlabel(r'Specific Angular Momentum (Relative to Circular)  $j_{z}/j_{\rm circ}$')
        
    return y_e, x


def center_and_clip(xyz,center_xyz,r_cut,n_target):
    '''
    trim vector, re-center it, and clip keeping particles of interest
    '''    
    xyz = xyz-center_xyz;
    d = np.amax(np.abs(xyz), axis=1)
    ok = np.where(d < r_cut)[0]
    xyz = xyz.take(ok, axis=0)
    if(xyz.shape[0] > n_target):
        ok_n = np.random.choice(xyz.shape[0], n_target)
        xyz = xyz.take(ok_n, axis=0)
        ok = ok.take(ok_n, axis=0)
    return xyz, ok


def estimate_zoom_center(sdir, snum, # snapshot directory and number
    ptype=1, # particle type to search for density peak in 
    min_searchsize=0.01, # minimum radius in code units to search iteratively
    search_deviation_tolerance=0.0005, # tolerance for change in center on subsequent iterations
    search_stepsize=1.25, # typical step-size for zoom-in iteration around density peak
    max_searchsize=1000., # initial maximum radius around median point to use for iteration
    cen_guess=[0.,0.,0.], # initial guess for centroid, to use for iterations (0,0,0) = no guess
    quiet=True, # suppress output from iterations
    **kwargs): # additional keyword arguments for check_if_filename_exists
    '''
    Quick routine to estimate the central mass concentration of a zoom-in snapshot
    '''
    
    fname,fname_base,fname_ext = check_if_filename_exists(sdir,snum,**kwargs)
    cen = np.array(cen_guess)
    if(fname=='NULL'): return cen, 0
    if(fname_ext!='.hdf5'): return cen, 0
    file = h5py.File(fname,'r') # Open hdf5 snapshot file
    header_toparse = file["Header"].attrs # Load header dictionary (to parse below)
    numfiles = header_toparse["NumFilesPerSnapshot"]
    npartTotal = header_toparse["NumPart_Total"]
    if(npartTotal[ptype]<1): return cen, 0    
    boxsize = header_toparse["BoxSize"]
    time = header_toparse["Time"]
    z = header_toparse["Redshift"]
    hubble = header_toparse["HubbleParam"]
    file.close()
    ascale=1.0; cosmological=False;
    if(np.abs(time*(1.+z)-1.) < 1.e-6): 
        cosmological=True; ascale=time;
    rcorr = hubble / ascale; # scale to code units
    max_searchsize *= rcorr; min_searchsize *= rcorr; search_deviation_tolerance *= rcorr;
    if(max_searchsize > boxsize): max_searchsize=boxsize
    d_thold = max_searchsize
    if(cen[0]==0.): d_thold=1.e10
    niter = 0
    xyz = np.zeros((0,3))
    while(1):
        xyz_m=np.zeros(3); n_t=np.zeros(1);
        if(niter == 0):
            for i_file in range(numfiles):
                if (numfiles>1): fname = fname_base+'.'+str(i_file)+fname_ext  
                if(os.stat(fname).st_size>0):
                    file = h5py.File(fname,'r') # Open hdf5 snapshot file
                    npart = file["Header"].attrs["NumPart_ThisFile"]
                    if(npart[ptype] > 1):
                        xyz_all = np.array(file['PartType'+str(ptype)+'/Coordinates/'])
                        d = np.amax(np.abs(xyz_all-cen),axis=1)
                        ok = np.where(d < d_thold)
                        n0 = ok[0].shape[0]
                        xyz_all = xyz_all.take(ok[0],axis=0)
                        if(xyz_all.size > 0): 
                            xyz = np.concatenate([xyz,xyz_all])
                            xyz_m += np.sum(xyz_all,axis=0)
                            n_t += n0
                    file.close()
            xyz_prev = xyz
            if xyz.size == 0:
                ## didn't load any particles -- go back and use a bigger threshold on initial load
                d_thold *= 3
                niter = 0
                continue
        else:
            d = np.amax(np.abs(xyz_prev-cen),axis=1)
            ok = np.where(d < d_thold)
            n0 = ok[0].shape[0]
            xyz = xyz_prev.take(ok[0],axis=0)
            if(n0 > 1):
                xyz_m += np.sum(xyz,axis=0)
                n_t += n0
        niter += 1;
        if(n_t[0] <= 0): 
            d_thold *= 1.5
        else:
            xyz_m /= n_t[0]
            d_cen = np.sqrt(np.sum((cen-xyz_m)**2))
            if(quiet==False): print('cen_o=',cen[0],cen[1],cen[2],' cen_n=',xyz_m[0],xyz_m[1],xyz_m[2],' in box=',d_thold,' cen_diff=',d_cen,' min_search/dev_tol=',min_searchsize,search_deviation_tolerance)
            if(niter > 100): break
            if(niter > 3):
                if(d_thold <= min_searchsize): break
                if(d_cen <= search_deviation_tolerance): break
            if(n_t[0] <= 10): break
            cen = xyz_m
            d_thold /= search_stepsize
            if(d_thold < 2.*d_cen): d_thold = 2.*d_cen
            if(max_searchsize < d_thold): d_thold=max_searchsize
            xyz_prev = xyz
    return xyz_m / rcorr, npartTotal[ptype]

