import numpy as np
import h5py as h5py
import os.path
import matplotlib
import cmocean
import matplotlib.pylab as pylab
import matplotlib.pyplot as plot
import scipy.interpolate as interpolate
from mpl_toolkits.mplot3d import Axes3D
import lic_internal


def test():
    pylab.close('all')

    dpi = 100
    size = 700

    vortex_spacing = 0.5
    extra_factor = 2.

    a = np.array([1,0])*vortex_spacing
    b = np.array([np.cos(np.pi/3),np.sin(np.pi/3)])*vortex_spacing
    rnv = int(2*extra_factor/vortex_spacing)
    vortices = [n*a+m*b for n in range(-rnv,rnv) for m in range(-rnv,rnv)]
    vortices = [(x,y) for (x,y) in vortices if -extra_factor<x<extra_factor and -extra_factor<y<extra_factor]


    xs = np.linspace(-1,1,size).astype(np.float32)[None,:]
    ys = np.linspace(-1,1,size).astype(np.float32)[:,None]

    vectors = np.zeros((size,size,2),dtype=np.float32)
    for (x,y) in vortices:
        rsq = (xs-x)**2+(ys-y)**2
        vectors[...,0] +=  (ys-y)/rsq
        vectors[...,1] += -(xs-x)/rsq
        
    size = 1024
    
    size_x = 512
    size_y = 1024
    y,x = np.mgrid[ 0:1:1j*size_y , 0:1:1j*size_x ]
    vx=(x-0.5); vy=(y-0.01);
    vectors = (np.stack((vx,vy),axis=2))
    print(vectors.shape)
    
    texture = np.random.rand(size_x,size_y)
    kernellen=31
    kernellen=101
    kernel = np.sin(np.arange(kernellen)*np.pi/kernellen)

    image = lic_internal.line_integral_convolution(vectors.astype(np.float32), texture.astype(np.float32), kernel.astype(np.float32))

    print(image.shape)

    #plt.clf()
    #plt.axis('off')
    #plt.figimage(image)
    #plt.gcf().set_size_inches((size/float(dpi),size/float(dpi)))
    #plt.savefig("flow-image.png",dpi=dpi)

    im=pylab.imshow(image,extent=(0,1,0,1),zorder=1,cmap='bone');




